package com.superoti.tasklist.web.rest;

import com.superoti.tasklist.TasklistApp;

import com.superoti.tasklist.domain.Tasklist;
import com.superoti.tasklist.repository.TasklistRepository;
import com.superoti.tasklist.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.superoti.tasklist.domain.enumeration.Status;
/**
 * Test class for the TasklistResource REST controller.
 *
 * @see TasklistResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TasklistApp.class)
public class TasklistResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.Aberto;
    private static final Status UPDATED_STATUS = Status.Processando;

    @Autowired
    private TasklistRepository tasklistRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTasklistMockMvc;

    private Tasklist tasklist;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TasklistResource tasklistResource = new TasklistResource(tasklistRepository);
        this.restTasklistMockMvc = MockMvcBuilders.standaloneSetup(tasklistResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tasklist createEntity(EntityManager em) {
        Tasklist tasklist = new Tasklist()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .status(DEFAULT_STATUS);
        return tasklist;
    }

    @Before
    public void initTest() {
        tasklist = createEntity(em);
    }

    @Test
    @Transactional
    public void createTasklist() throws Exception {
        int databaseSizeBeforeCreate = tasklistRepository.findAll().size();

        // Create the Tasklist
        restTasklistMockMvc.perform(post("/api/tasklists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tasklist)))
            .andExpect(status().isCreated());

        // Validate the Tasklist in the database
        List<Tasklist> tasklistList = tasklistRepository.findAll();
        assertThat(tasklistList).hasSize(databaseSizeBeforeCreate + 1);
        Tasklist testTasklist = tasklistList.get(tasklistList.size() - 1);
        assertThat(testTasklist.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testTasklist.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTasklist.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createTasklistWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tasklistRepository.findAll().size();

        // Create the Tasklist with an existing ID
        tasklist.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTasklistMockMvc.perform(post("/api/tasklists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tasklist)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Tasklist> tasklistList = tasklistRepository.findAll();
        assertThat(tasklistList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = tasklistRepository.findAll().size();
        // set the field null
        tasklist.setTitle(null);

        // Create the Tasklist, which fails.

        restTasklistMockMvc.perform(post("/api/tasklists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tasklist)))
            .andExpect(status().isBadRequest());

        List<Tasklist> tasklistList = tasklistRepository.findAll();
        assertThat(tasklistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTasklists() throws Exception {
        // Initialize the database
        tasklistRepository.saveAndFlush(tasklist);

        // Get all the tasklistList
        restTasklistMockMvc.perform(get("/api/tasklists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tasklist.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getTasklist() throws Exception {
        // Initialize the database
        tasklistRepository.saveAndFlush(tasklist);

        // Get the tasklist
        restTasklistMockMvc.perform(get("/api/tasklists/{id}", tasklist.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tasklist.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTasklist() throws Exception {
        // Get the tasklist
        restTasklistMockMvc.perform(get("/api/tasklists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTasklist() throws Exception {
        // Initialize the database
        tasklistRepository.saveAndFlush(tasklist);
        int databaseSizeBeforeUpdate = tasklistRepository.findAll().size();

        // Update the tasklist
        Tasklist updatedTasklist = tasklistRepository.findOne(tasklist.getId());
        updatedTasklist
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .status(UPDATED_STATUS);

        restTasklistMockMvc.perform(put("/api/tasklists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTasklist)))
            .andExpect(status().isOk());

        // Validate the Tasklist in the database
        List<Tasklist> tasklistList = tasklistRepository.findAll();
        assertThat(tasklistList).hasSize(databaseSizeBeforeUpdate);
        Tasklist testTasklist = tasklistList.get(tasklistList.size() - 1);
        assertThat(testTasklist.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testTasklist.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTasklist.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingTasklist() throws Exception {
        int databaseSizeBeforeUpdate = tasklistRepository.findAll().size();

        // Create the Tasklist

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTasklistMockMvc.perform(put("/api/tasklists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tasklist)))
            .andExpect(status().isCreated());

        // Validate the Tasklist in the database
        List<Tasklist> tasklistList = tasklistRepository.findAll();
        assertThat(tasklistList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTasklist() throws Exception {
        // Initialize the database
        tasklistRepository.saveAndFlush(tasklist);
        int databaseSizeBeforeDelete = tasklistRepository.findAll().size();

        // Get the tasklist
        restTasklistMockMvc.perform(delete("/api/tasklists/{id}", tasklist.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Tasklist> tasklistList = tasklistRepository.findAll();
        assertThat(tasklistList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tasklist.class);
        Tasklist tasklist1 = new Tasklist();
        tasklist1.setId(1L);
        Tasklist tasklist2 = new Tasklist();
        tasklist2.setId(tasklist1.getId());
        assertThat(tasklist1).isEqualTo(tasklist2);
        tasklist2.setId(2L);
        assertThat(tasklist1).isNotEqualTo(tasklist2);
        tasklist1.setId(null);
        assertThat(tasklist1).isNotEqualTo(tasklist2);
    }
}
