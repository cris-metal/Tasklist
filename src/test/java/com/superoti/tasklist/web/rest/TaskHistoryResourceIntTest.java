package com.superoti.tasklist.web.rest;

import com.superoti.tasklist.TasklistApp;

import com.superoti.tasklist.domain.TaskHistory;
import com.superoti.tasklist.domain.Tasklist;
import com.superoti.tasklist.domain.User;
import com.superoti.tasklist.repository.TaskHistoryRepository;
import com.superoti.tasklist.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.superoti.tasklist.domain.enumeration.Status;
/**
 * Test class for the TaskHistoryResource REST controller.
 *
 * @see TaskHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TasklistApp.class)
public class TaskHistoryResourceIntTest {

    private static final LocalDate DEFAULT_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CONCLUSION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CONCLUSION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EXCLUSION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXCLUSION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Status DEFAULT_STATUS = Status.Aberto;
    private static final Status UPDATED_STATUS = Status.Processando;

    @Autowired
    private TaskHistoryRepository taskHistoryRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTaskHistoryMockMvc;

    private TaskHistory taskHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TaskHistoryResource taskHistoryResource = new TaskHistoryResource(taskHistoryRepository);
        this.restTaskHistoryMockMvc = MockMvcBuilders.standaloneSetup(taskHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TaskHistory createEntity(EntityManager em) {
        TaskHistory taskHistory = new TaskHistory()
            .updateDate(DEFAULT_UPDATE_DATE)
            .conclusionDate(DEFAULT_CONCLUSION_DATE)
            .exclusionDate(DEFAULT_EXCLUSION_DATE)
            .creationDate(DEFAULT_CREATION_DATE)
            .status(DEFAULT_STATUS);
        // Add required entity
        Tasklist tasklist = TasklistResourceIntTest.createEntity(em);
        em.persist(tasklist);
        em.flush();
        taskHistory.setTasklist(tasklist);
        // Add required entity
        User user = UserResourceIntTest.createEntity(em);
        em.persist(user);
        em.flush();
        taskHistory.setUser(user);
        return taskHistory;
    }

    @Before
    public void initTest() {
        taskHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createTaskHistory() throws Exception {
        int databaseSizeBeforeCreate = taskHistoryRepository.findAll().size();

        // Create the TaskHistory
        restTaskHistoryMockMvc.perform(post("/api/task-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(taskHistory)))
            .andExpect(status().isCreated());

        // Validate the TaskHistory in the database
        List<TaskHistory> taskHistoryList = taskHistoryRepository.findAll();
        assertThat(taskHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        TaskHistory testTaskHistory = taskHistoryList.get(taskHistoryList.size() - 1);
        assertThat(testTaskHistory.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testTaskHistory.getConclusionDate()).isEqualTo(DEFAULT_CONCLUSION_DATE);
        assertThat(testTaskHistory.getExclusionDate()).isEqualTo(DEFAULT_EXCLUSION_DATE);
        assertThat(testTaskHistory.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testTaskHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createTaskHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = taskHistoryRepository.findAll().size();

        // Create the TaskHistory with an existing ID
        taskHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTaskHistoryMockMvc.perform(post("/api/task-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(taskHistory)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TaskHistory> taskHistoryList = taskHistoryRepository.findAll();
        assertThat(taskHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTaskHistories() throws Exception {
        // Initialize the database
        taskHistoryRepository.saveAndFlush(taskHistory);

        // Get all the taskHistoryList
        restTaskHistoryMockMvc.perform(get("/api/task-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(taskHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].conclusionDate").value(hasItem(DEFAULT_CONCLUSION_DATE.toString())))
            .andExpect(jsonPath("$.[*].exclusionDate").value(hasItem(DEFAULT_EXCLUSION_DATE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getTaskHistory() throws Exception {
        // Initialize the database
        taskHistoryRepository.saveAndFlush(taskHistory);

        // Get the taskHistory
        restTaskHistoryMockMvc.perform(get("/api/task-histories/{id}", taskHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(taskHistory.getId().intValue()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.conclusionDate").value(DEFAULT_CONCLUSION_DATE.toString()))
            .andExpect(jsonPath("$.exclusionDate").value(DEFAULT_EXCLUSION_DATE.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTaskHistory() throws Exception {
        // Get the taskHistory
        restTaskHistoryMockMvc.perform(get("/api/task-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTaskHistory() throws Exception {
        // Initialize the database
        taskHistoryRepository.saveAndFlush(taskHistory);
        int databaseSizeBeforeUpdate = taskHistoryRepository.findAll().size();

        // Update the taskHistory
        TaskHistory updatedTaskHistory = taskHistoryRepository.findOne(taskHistory.getId());
        updatedTaskHistory
            .updateDate(UPDATED_UPDATE_DATE)
            .conclusionDate(UPDATED_CONCLUSION_DATE)
            .exclusionDate(UPDATED_EXCLUSION_DATE)
            .creationDate(UPDATED_CREATION_DATE)
            .status(UPDATED_STATUS);

        restTaskHistoryMockMvc.perform(put("/api/task-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTaskHistory)))
            .andExpect(status().isOk());

        // Validate the TaskHistory in the database
        List<TaskHistory> taskHistoryList = taskHistoryRepository.findAll();
        assertThat(taskHistoryList).hasSize(databaseSizeBeforeUpdate);
        TaskHistory testTaskHistory = taskHistoryList.get(taskHistoryList.size() - 1);
        assertThat(testTaskHistory.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testTaskHistory.getConclusionDate()).isEqualTo(UPDATED_CONCLUSION_DATE);
        assertThat(testTaskHistory.getExclusionDate()).isEqualTo(UPDATED_EXCLUSION_DATE);
        assertThat(testTaskHistory.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testTaskHistory.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingTaskHistory() throws Exception {
        int databaseSizeBeforeUpdate = taskHistoryRepository.findAll().size();

        // Create the TaskHistory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTaskHistoryMockMvc.perform(put("/api/task-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(taskHistory)))
            .andExpect(status().isCreated());

        // Validate the TaskHistory in the database
        List<TaskHistory> taskHistoryList = taskHistoryRepository.findAll();
        assertThat(taskHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTaskHistory() throws Exception {
        // Initialize the database
        taskHistoryRepository.saveAndFlush(taskHistory);
        int databaseSizeBeforeDelete = taskHistoryRepository.findAll().size();

        // Get the taskHistory
        restTaskHistoryMockMvc.perform(delete("/api/task-histories/{id}", taskHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TaskHistory> taskHistoryList = taskHistoryRepository.findAll();
        assertThat(taskHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TaskHistory.class);
        TaskHistory taskHistory1 = new TaskHistory();
        taskHistory1.setId(1L);
        TaskHistory taskHistory2 = new TaskHistory();
        taskHistory2.setId(taskHistory1.getId());
        assertThat(taskHistory1).isEqualTo(taskHistory2);
        taskHistory2.setId(2L);
        assertThat(taskHistory1).isNotEqualTo(taskHistory2);
        taskHistory1.setId(null);
        assertThat(taskHistory1).isNotEqualTo(taskHistory2);
    }
}
