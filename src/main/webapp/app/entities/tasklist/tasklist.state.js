(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tasklist', {
            parent: 'entity',
            url: '/tasklist?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tasklistApp.tasklist.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tasklist/tasklists.html',
                    controller: 'TasklistController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tasklist');
                    $translatePartialLoader.addPart('status');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('tasklist-detail', {
            parent: 'tasklist',
            url: '/tasklist/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tasklistApp.tasklist.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tasklist/tasklist-detail.html',
                    controller: 'TasklistDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tasklist');
                    $translatePartialLoader.addPart('status');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Tasklist', function($stateParams, Tasklist) {
                    return Tasklist.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'tasklist',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('tasklist-detail.edit', {
            parent: 'tasklist-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tasklist/tasklist-dialog.html',
                    controller: 'TasklistDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Tasklist', function(Tasklist) {
                            return Tasklist.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tasklist.new', {
            parent: 'tasklist',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tasklist/tasklist-dialog.html',
                    controller: 'TasklistDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                description: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tasklist', null, { reload: 'tasklist' });
                }, function() {
                    $state.go('tasklist');
                });
            }]
        })
        .state('tasklist.edit', {
            parent: 'tasklist',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tasklist/tasklist-dialog.html',
                    controller: 'TasklistDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Tasklist', function(Tasklist) {
                            return Tasklist.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tasklist', null, { reload: 'tasklist' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tasklist.delete', {
            parent: 'tasklist',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tasklist/tasklist-delete-dialog.html',
                    controller: 'TasklistDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Tasklist', function(Tasklist) {
                            return Tasklist.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tasklist', null, { reload: 'tasklist' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
