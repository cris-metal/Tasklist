(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .controller('TasklistDialogController', TasklistDialogController);

    TasklistDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tasklist'];

    function TasklistDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Tasklist) {
        var vm = this;

        vm.tasklist = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tasklist.id !== null) {
                Tasklist.update(vm.tasklist, onSaveSuccess, onSaveError);
            } else {
                Tasklist.save(vm.tasklist, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('tasklistApp:tasklistUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
