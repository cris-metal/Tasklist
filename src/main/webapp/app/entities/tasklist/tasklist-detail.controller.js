(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .controller('TasklistDetailController', TasklistDetailController);

    TasklistDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Tasklist'];

    function TasklistDetailController($scope, $rootScope, $stateParams, previousState, entity, Tasklist) {
        var vm = this;

        vm.tasklist = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('tasklistApp:tasklistUpdate', function(event, result) {
            vm.tasklist = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
