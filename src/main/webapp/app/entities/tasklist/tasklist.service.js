(function() {
    'use strict';
    angular
        .module('tasklistApp')
        .factory('Tasklist', Tasklist);

    Tasklist.$inject = ['$resource'];

    function Tasklist ($resource) {
        var resourceUrl =  'api/tasklists/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
