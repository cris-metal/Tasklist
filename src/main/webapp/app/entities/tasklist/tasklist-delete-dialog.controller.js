(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .controller('TasklistDeleteController',TasklistDeleteController);

    TasklistDeleteController.$inject = ['$uibModalInstance', 'entity', 'Tasklist'];

    function TasklistDeleteController($uibModalInstance, entity, Tasklist) {
        var vm = this;

        vm.tasklist = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Tasklist.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
