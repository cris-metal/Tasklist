(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('task-history', {
            parent: 'entity',
            url: '/task-history?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tasklistApp.taskHistory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/task-history/task-histories.html',
                    controller: 'TaskHistoryController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('taskHistory');
                    $translatePartialLoader.addPart('status');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('task-history-detail', {
            parent: 'task-history',
            url: '/task-history/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'tasklistApp.taskHistory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/task-history/task-history-detail.html',
                    controller: 'TaskHistoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('taskHistory');
                    $translatePartialLoader.addPart('status');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TaskHistory', function($stateParams, TaskHistory) {
                    return TaskHistory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'task-history',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('task-history-detail.edit', {
            parent: 'task-history-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-history/task-history-dialog.html',
                    controller: 'TaskHistoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TaskHistory', function(TaskHistory) {
                            return TaskHistory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('task-history.new', {
            parent: 'task-history',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-history/task-history-dialog.html',
                    controller: 'TaskHistoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                updateDate: null,
                                conclusionDate: null,
                                exclusionDate: null,
                                creationDate: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('task-history', null, { reload: 'task-history' });
                }, function() {
                    $state.go('task-history');
                });
            }]
        })
        .state('task-history.edit', {
            parent: 'task-history',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-history/task-history-dialog.html',
                    controller: 'TaskHistoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TaskHistory', function(TaskHistory) {
                            return TaskHistory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('task-history', null, { reload: 'task-history' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('task-history.delete', {
            parent: 'task-history',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-history/task-history-delete-dialog.html',
                    controller: 'TaskHistoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TaskHistory', function(TaskHistory) {
                            return TaskHistory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('task-history', null, { reload: 'task-history' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
