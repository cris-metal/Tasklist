(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .controller('TaskHistoryDetailController', TaskHistoryDetailController);

    TaskHistoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TaskHistory', 'Tasklist', 'User'];

    function TaskHistoryDetailController($scope, $rootScope, $stateParams, previousState, entity, TaskHistory, Tasklist, User) {
        var vm = this;

        vm.taskHistory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('tasklistApp:taskHistoryUpdate', function(event, result) {
            vm.taskHistory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
