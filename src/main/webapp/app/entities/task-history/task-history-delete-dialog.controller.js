(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .controller('TaskHistoryDeleteController',TaskHistoryDeleteController);

    TaskHistoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'TaskHistory'];

    function TaskHistoryDeleteController($uibModalInstance, entity, TaskHistory) {
        var vm = this;

        vm.taskHistory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TaskHistory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
