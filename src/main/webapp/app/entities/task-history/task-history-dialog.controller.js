(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .controller('TaskHistoryDialogController', TaskHistoryDialogController);

    TaskHistoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TaskHistory', 'Tasklist', 'User'];

    function TaskHistoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TaskHistory, Tasklist, User) {
        var vm = this;

        vm.taskHistory = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.tasklists = Tasklist.query();
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.taskHistory.id !== null) {
                TaskHistory.update(vm.taskHistory, onSaveSuccess, onSaveError);
            } else {
                TaskHistory.save(vm.taskHistory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('tasklistApp:taskHistoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.updateDate = false;
        vm.datePickerOpenStatus.conclusionDate = false;
        vm.datePickerOpenStatus.exclusionDate = false;
        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
