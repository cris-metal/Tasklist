(function() {
    'use strict';
    angular
        .module('tasklistApp')
        .factory('TaskHistory', TaskHistory);

    TaskHistory.$inject = ['$resource', 'DateUtils'];

    function TaskHistory ($resource, DateUtils) {
        var resourceUrl =  'api/task-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.updateDate = DateUtils.convertLocalDateFromServer(data.updateDate);
                        data.conclusionDate = DateUtils.convertLocalDateFromServer(data.conclusionDate);
                        data.exclusionDate = DateUtils.convertLocalDateFromServer(data.exclusionDate);
                        data.creationDate = DateUtils.convertLocalDateFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.updateDate = DateUtils.convertLocalDateToServer(copy.updateDate);
                    copy.conclusionDate = DateUtils.convertLocalDateToServer(copy.conclusionDate);
                    copy.exclusionDate = DateUtils.convertLocalDateToServer(copy.exclusionDate);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.updateDate = DateUtils.convertLocalDateToServer(copy.updateDate);
                    copy.conclusionDate = DateUtils.convertLocalDateToServer(copy.conclusionDate);
                    copy.exclusionDate = DateUtils.convertLocalDateToServer(copy.exclusionDate);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
