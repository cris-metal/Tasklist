(function() {
    'use strict';

    angular
        .module('tasklistApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
