package com.superoti.tasklist.service;

import com.superoti.tasklist.domain.TaskHistory;
import com.superoti.tasklist.domain.Tasklist;
import com.superoti.tasklist.domain.User;
import com.superoti.tasklist.domain.enumeration.Status;
import com.superoti.tasklist.repository.TaskHistoryRepository;
import com.superoti.tasklist.repository.UserRepository;
import com.superoti.tasklist.security.SecurityUtils;

import java.time.LocalDate;

/**
 * Created by cris on 28/07/2017.
 */
public class TaskHistoryService {


    /**
     * Seta os valores da history
     * @param tasklist
     * @param status
     * @param userRepository
     * @param taskHistoryRepository
     * @return
     */
    public static boolean saveTaskHistory(Tasklist tasklist, Status status, UserRepository userRepository, TaskHistoryRepository taskHistoryRepository){
        String username = SecurityUtils.getCurrentUserLogin();
        User user = userRepository.findOneByLogin(username).get();

        TaskHistory taskHistory = new TaskHistory();
        taskHistory.setTasklist(tasklist);
        taskHistory.setStatus(status);
        taskHistory.setUser(user);

        setTaskHistoryDates(taskHistory, status);
        taskHistoryRepository.save(taskHistory);

        return true;
    }

    /**
     * Seta a data conforme o status q esta sendo sendo alterado
     */
    private static TaskHistory setTaskHistoryDates(TaskHistory taskHistory, Status status){
        switch (status) {
            case Aberto:
                taskHistory.setCreationDate(LocalDate.now());

            case Cancelado:
            case Processando:
                taskHistory.setUpdateDate(LocalDate.now());

            case Fechado:
                taskHistory.setConclusionDate(LocalDate.now());

            case Excluido:
                taskHistory.setExclusionDate(LocalDate.now());
        }

        return taskHistory;
    }
}
