package com.superoti.tasklist.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    Aberto, Processando, Cancelado, Fechado, Excluido
}
