package com.superoti.tasklist.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.superoti.tasklist.domain.enumeration.Status;

/**
 * A TaskHistory.
 */
@Entity
@Table(name = "task_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TaskHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "conclusion_date")
    private LocalDate conclusionDate;

    @Column(name = "exclusion_date")
    private LocalDate exclusionDate;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @ManyToOne(optional = false)
    @NotNull
    private Tasklist tasklist;

    @ManyToOne(optional = false)
    @NotNull
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public TaskHistory updateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public LocalDate getConclusionDate() {
        return conclusionDate;
    }

    public TaskHistory conclusionDate(LocalDate conclusionDate) {
        this.conclusionDate = conclusionDate;
        return this;
    }

    public void setConclusionDate(LocalDate conclusionDate) {
        this.conclusionDate = conclusionDate;
    }

    public LocalDate getExclusionDate() {
        return exclusionDate;
    }

    public TaskHistory exclusionDate(LocalDate exclusionDate) {
        this.exclusionDate = exclusionDate;
        return this;
    }

    public void setExclusionDate(LocalDate exclusionDate) {
        this.exclusionDate = exclusionDate;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public TaskHistory creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Status getStatus() {
        return status;
    }

    public TaskHistory status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Tasklist getTasklist() {
        return tasklist;
    }

    public TaskHistory tasklist(Tasklist tasklist) {
        this.tasklist = tasklist;
        return this;
    }

    public void setTasklist(Tasklist tasklist) {
        this.tasklist = tasklist;
    }

    public User getUser() {
        return user;
    }

    public TaskHistory user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TaskHistory taskHistory = (TaskHistory) o;
        if (taskHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), taskHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TaskHistory{" +
            "id=" + getId() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", conclusionDate='" + getConclusionDate() + "'" +
            ", exclusionDate='" + getExclusionDate() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
