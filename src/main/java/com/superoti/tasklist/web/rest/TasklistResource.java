package com.superoti.tasklist.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.superoti.tasklist.domain.Tasklist;
import com.superoti.tasklist.domain.enumeration.Status;
import com.superoti.tasklist.repository.TaskHistoryRepository;
import com.superoti.tasklist.repository.TasklistRepository;
import com.superoti.tasklist.repository.UserRepository;
import com.superoti.tasklist.service.TaskHistoryService;
import com.superoti.tasklist.web.rest.util.HeaderUtil;
import com.superoti.tasklist.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tasklist.
 */
@RestController
@RequestMapping("/api")
public class TasklistResource {

    private final Logger log = LoggerFactory.getLogger(TasklistResource.class);

    private static final String ENTITY_NAME = "tasklist";

    private final TasklistRepository tasklistRepository;

    @Autowired
    private TaskHistoryRepository taskHistoryRepository;

    @Autowired
    private UserRepository userRepository;

    public TasklistResource(TasklistRepository tasklistRepository) {
        this.tasklistRepository = tasklistRepository;
    }

    /**
     * POST  /tasklists : Create a new tasklist.
     *
     * @param tasklist the tasklist to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tasklist, or with status 400 (Bad Request) if the tasklist has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tasklists")
    @Timed
    public ResponseEntity<Tasklist> createTasklist(@Valid @RequestBody Tasklist tasklist) throws URISyntaxException {
        log.debug("REST request to save Tasklist : {}", tasklist);
        if (tasklist.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tasklist cannot already have an ID")).body(null);
        }

        Tasklist result = tasklistRepository.save(tasklist);

        TaskHistoryService.saveTaskHistory(tasklist, Status.Aberto, userRepository, taskHistoryRepository);

        return ResponseEntity.created(new URI("/api/tasklists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tasklists : Updates an existing tasklist.
     *
     * @param tasklist the tasklist to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tasklist,
     * or with status 400 (Bad Request) if the tasklist is not valid,
     * or with status 500 (Internal Server Error) if the tasklist couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tasklists")
    @Timed
    public ResponseEntity<Tasklist> updateTasklist(@Valid @RequestBody Tasklist tasklist) throws URISyntaxException {
        log.debug("REST request to update Tasklist : {}", tasklist);
        if (tasklist.getId() == null) {
            return createTasklist(tasklist);
        }
        Tasklist result = tasklistRepository.save(tasklist);

        TaskHistoryService.saveTaskHistory(result, tasklist.getStatus(), userRepository, taskHistoryRepository);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tasklist.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tasklists : get all the tasklists.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tasklists in body
     */
    @GetMapping("/tasklists")
    @Timed
    public ResponseEntity<List<Tasklist>> getAllTasklists(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Tasklists");
        Page<Tasklist> page = tasklistRepository.findAllNotDeleted(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tasklists");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tasklists/:id : get the "id" tasklist.
     *
     * @param id the id of the tasklist to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tasklist, or with status 404 (Not Found)
     */
    @GetMapping("/tasklists/{id}")
    @Timed
    public ResponseEntity<Tasklist> getTasklist(@PathVariable Long id) {
        log.debug("REST request to get Tasklist : {}", id);
        Tasklist tasklist = tasklistRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tasklist));
    }

    /**
     * Esta exclusão será lógica, definida pelo status excluido
     *
     * DELETE  /tasklists/:id : delete the "id" tasklist.
     *
     * @param id the id of the tasklist to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tasklists/{id}")
    @Timed
    public ResponseEntity<Void> deleteTasklist(@PathVariable Long id) {
        log.debug("REST request to delete Tasklist : {}", id);
        Tasklist tasklist = tasklistRepository.findOne(id);

        tasklist.setStatus(Status.Excluido);
        tasklistRepository.save(tasklist);

        TaskHistoryService.saveTaskHistory(tasklist, Status.Excluido, userRepository, taskHistoryRepository);

        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
