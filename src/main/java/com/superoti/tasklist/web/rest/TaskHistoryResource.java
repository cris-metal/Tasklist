package com.superoti.tasklist.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.superoti.tasklist.domain.TaskHistory;

import com.superoti.tasklist.repository.TaskHistoryRepository;
import com.superoti.tasklist.web.rest.util.HeaderUtil;
import com.superoti.tasklist.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TaskHistory.
 */
@RestController
@RequestMapping("/api")
public class TaskHistoryResource {

    private final Logger log = LoggerFactory.getLogger(TaskHistoryResource.class);

    private static final String ENTITY_NAME = "taskHistory";

    private final TaskHistoryRepository taskHistoryRepository;

    public TaskHistoryResource(TaskHistoryRepository taskHistoryRepository) {
        this.taskHistoryRepository = taskHistoryRepository;
    }

    /**
     * POST  /task-histories : Create a new taskHistory.
     *
     * @param taskHistory the taskHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new taskHistory, or with status 400 (Bad Request) if the taskHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/task-histories")
    @Timed
    public ResponseEntity<TaskHistory> createTaskHistory(@Valid @RequestBody TaskHistory taskHistory) throws URISyntaxException {
        log.debug("REST request to save TaskHistory : {}", taskHistory);
        if (taskHistory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new taskHistory cannot already have an ID")).body(null);
        }
        TaskHistory result = taskHistoryRepository.save(taskHistory);
        return ResponseEntity.created(new URI("/api/task-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /task-histories : Updates an existing taskHistory.
     *
     * @param taskHistory the taskHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated taskHistory,
     * or with status 400 (Bad Request) if the taskHistory is not valid,
     * or with status 500 (Internal Server Error) if the taskHistory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/task-histories")
    @Timed
    public ResponseEntity<TaskHistory> updateTaskHistory(@Valid @RequestBody TaskHistory taskHistory) throws URISyntaxException {
        log.debug("REST request to update TaskHistory : {}", taskHistory);
        if (taskHistory.getId() == null) {
            return createTaskHistory(taskHistory);
        }
        TaskHistory result = taskHistoryRepository.save(taskHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, taskHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /task-histories : get all the taskHistories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of taskHistories in body
     */
    @GetMapping("/task-histories")
    @Timed
    public ResponseEntity<List<TaskHistory>> getAllTaskHistories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TaskHistories");
        Page<TaskHistory> page = taskHistoryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/task-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /task-histories/:id : get the "id" taskHistory.
     *
     * @param id the id of the taskHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the taskHistory, or with status 404 (Not Found)
     */
    @GetMapping("/task-histories/{id}")
    @Timed
    public ResponseEntity<TaskHistory> getTaskHistory(@PathVariable Long id) {
        log.debug("REST request to get TaskHistory : {}", id);
        TaskHistory taskHistory = taskHistoryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(taskHistory));
    }

    /**
     * DELETE  /task-histories/:id : delete the "id" taskHistory.
     *
     * @param id the id of the taskHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/task-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteTaskHistory(@PathVariable Long id) {
        log.debug("REST request to delete TaskHistory : {}", id);
        taskHistoryRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
