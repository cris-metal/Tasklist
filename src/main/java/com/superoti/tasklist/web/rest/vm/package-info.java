/**
 * View Models used by Spring MVC REST controllers.
 */
package com.superoti.tasklist.web.rest.vm;
