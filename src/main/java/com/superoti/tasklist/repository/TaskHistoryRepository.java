package com.superoti.tasklist.repository;

import com.superoti.tasklist.domain.TaskHistory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the TaskHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TaskHistoryRepository extends JpaRepository<TaskHistory,Long> {

    @Query("select task_history from TaskHistory task_history where task_history.user.login = ?#{principal.username}")
    List<TaskHistory> findByUserIsCurrentUser();
    
}
