package com.superoti.tasklist.repository;

import com.superoti.tasklist.domain.TaskHistory;
import com.superoti.tasklist.domain.Tasklist;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Tasklist entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TasklistRepository extends JpaRepository<Tasklist,Long> {
    @Query("select tasklist from Tasklist tasklist where tasklist.status <> 'Excluido'")
    Page<Tasklist> findAllNotDeleted(Pageable pageable);

}
